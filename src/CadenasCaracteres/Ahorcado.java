package CadenasCaracteres;

import java.util.concurrent.ThreadLocalRandom;
import java.util.Scanner;

/**
 * Juego del Ahorcado
 *
 * @author rocio mera
 */
public class Ahorcado {

    public static void main(String args[]) {
        String[] listaPalabras = {"programar", "es", "divertido", "java", "rules"};
        int numFails = 5;
        int currentNumFails = 0;
        boolean gano = false;

        // obtener una palabra al azar
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = ThreadLocalRandom.current().nextInt(0, listaPalabras.length);
        //palabra a adivinar por el usuario
        String palabra = listaPalabras[randomNum];

        //variable que tiene los caracteres que el usuario a mostrado hasta el momento
        char[] caracteresAdivinados = new char[palabra.length()];
        for (int i = 0; i < palabra.length(); i++) {
            caracteresAdivinados[i] = '*';
        }
        String palabraOculta = new String(caracteresAdivinados);

        Scanner entrada = new Scanner(System.in);
        while (currentNumFails < numFails && !gano) {

            //mostrar palabra al usuario
            System.out.printf("Palabra a adivinar: %s\n", palabraOculta);
            System.out.print("Ingrese una letra: ");

            boolean letraExiste = false;

            //pedir al usuario que ingrese la letra
            String ingresoUsuario = entrada.nextLine();
            char letra = ingresoUsuario.charAt(0);

            //ver si la letra se encuentra en la palabra
            //si se encuentra reemplazar en los caracteres adivinados
            for (int i = 0; i < palabra.length(); i++) {
                if (palabra.charAt(i) == letra && caracteresAdivinados[i] != letra) {
                    caracteresAdivinados[i] = letra;
                    letraExiste = true;
                }
            }
            palabraOculta = new String(caracteresAdivinados);

            if (!letraExiste) {
                currentNumFails++;
                System.out.println("La letra ingresada no existe");
                System.out.printf("Le quedan %d de %d intentos\n", (numFails - currentNumFails), numFails);
            } else if (palabraOculta.equals(palabra)) {
                gano = true;
            }
        }

        //cerramos el objeto Scanner
        entrada.close();
        if (gano) {
            System.out.printf("Usted gano en %d intentos %n", currentNumFails);
        } else {
            System.out.printf("Usted perdio");
        }

    }//end method main
}//end class Ahorcado.java

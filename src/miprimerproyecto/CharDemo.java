/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;

/**
 *
 * @author rociomera
 */
public class CharDemo {
    public static void main(String args[]) {        			
        char ch1, ch2;    //declaración de las variables ch1 y ch2 de tipo char    
        ch1 = 88; // unicode para 'X'        
        ch2 = 'C';        
        System.out.print("ch1 and ch2: ");        
        System.out.println(ch1 + " " + ch2);   
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;

/* 
 * La Clase MiPrimerPrograma implementa una aplicación 
 * que muestra "¡Hola Mundo!" como salida 
 */ 
public class MiPrimerPrograma {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Muestra "¡Hola Mundo!" 
	System.out.println("¡Hola Mundo!");
    }
}

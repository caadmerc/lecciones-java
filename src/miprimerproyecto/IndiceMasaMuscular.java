/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;
import java.util.Scanner; // program uses class Scanner

/**
 * Programa que sirve para calcular el índice de masa muscular de una 
 * persona e indicar si esa persona tiene o no sobre peso
 * imc = peso / (altura * altura);
 * imc <  18.5, peso bajo
 * imc entre y 24.99, Peso Normal
 * imc > 24.99, sobre peso
 * @author rociomera
 */
public class IndiceMasaMuscular {
    public static void main(String[] args){
        // crea un objeto de tipo Scanner para poder obtener datos de la consola
        Scanner ingreso = new Scanner( System.in );
        
        //la funcion print es similar a println, solo que no agrega 
        //un salto de linea al final
        System.out.print("Ingrese su nombre: ");
        //uso nextLine en lugar de next, porque next lee hasta que encuentra un 
        //caracter vacio, mienstras que nextLine lee hasta que encuentra el salto
        //de linea
        String nombre = ingreso.nextLine(); 
        
        System.out.print("Ingrese peso (kg): ");        
        //pedimos el peso
        float peso = ingreso.nextFloat();
        
        System.out.print("Ingrese altura (m): "); 
        float altura = ingreso.nextFloat();
        
        ingreso.close();

        //imc debe ser double porque la funcion Math.sqrt retorna 
        //un tipo de dato double. El resultado de la division seria
        //double y no se puede asignar una variable double a un float
        //porque hay perdida de presicion
        double imc = peso / Math.sqrt(altura);

        if(imc < 18.5){
            System.out.println(nombre+" tiene peso Bajo");
        }else if(imc >= 18.5 && imc <= 24.99){
            System.out.println(nombre+" tiene peso normal");
        }else
            System.out.println(nombre+" tiene sobre peso");
            System.out.println("test 1");
            System.out.println("test 2");
        
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;
import java.util.Scanner;

/**
 *
 * @author rociomera
 */
public class ConsultaMedicaIngreso {
    
    public static void main(String[] args){
        Scanner ingreso = new Scanner(System.in);
        
        System.out.print("Ingrese nombre: ");
        String nombre=ingreso.nextLine();
        System.out.print("Ingrese identificador: ");
        String id=ingreso.nextLine();
        System.out.print("Ingrese edad: ");
        int edad=ingreso.nextInt();
        System.out.print("Ingrese sexo (M/F): ");
        char sexo = ingreso.next().charAt(0);
        System.out.print("Ingrese estatura (m): ");
        float estatura = ingreso.nextFloat();
        System.out.print("Ingrese peso (kg): ");
        float peso = ingreso.nextFloat();
        ingreso.close();
        
        double imc = peso / (estatura * estatura);

        System.out.println("Nombre: "+nombre);
        System.out.println("Id: "+id);
        System.out.println("Edad: "+edad);
        System.out.println("Sexo: "+sexo);
        System.out.println("Estatura: "+estatura);
        System.out.println("Peso: "+peso);
        System.out.println("Imc: "+imc);
        
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;
import java.util.Scanner;

/**
 *
 * @author rociomera
 */
public class SietePum {
    public static void main(String[] args){
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese numero: ");
        int limite = ingreso.nextInt();
        for(int x=1;x<limite+1;x++){
            if(x%7 == 0 || x%10==7){
                System.out.print("pum ");
            }else{
                System.out.print(x+" ");
            }
        }
        ingreso.close();
    }
}

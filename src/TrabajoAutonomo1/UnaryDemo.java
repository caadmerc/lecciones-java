package TrabajoAutonomo1;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class UnaryDemo {

    public static void main(String[] args) {
        System.out.println("CARLOS MERCHAN");

        double resultado = +1;
        System.out.println(resultado);

        resultado--;
        System.out.println(resultado);

        resultado++;
        System.out.println(resultado);

        resultado = -resultado;
        System.out.println(resultado);

        boolean success = false;
        System.out.println(success);
        System.out.println(!success);
    }//end method main
}//end class UnaryDemo

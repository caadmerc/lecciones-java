package TrabajoAutonomo1;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class PrePostDemo {

    public static void main(String[] args) {
        System.out.println("CARLOS MERCHAN");

        int k = 9;
        k++;
        System.out.println(k);
        ++k;
        System.out.println(k);
        System.out.println(++k);
        System.out.println(k++);
        System.out.println(k);
    }//end method main
}//end class PrePostDemo       

package TrabajoAutonomo1;

import java.util.Scanner;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class ConsultaMedica {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese nombre: ");
        String nombre = input.nextLine();
        System.out.print("Ingrese id: ");
        String id = input.nextLine();
        System.out.print("Ingrese edad: ");
        int edad = input.nextInt();
        System.out.print("Ingrese sexo (F)(M): ");
        char sexo = input.next().charAt(0);
        System.out.print("Ingrese peso (kg): ");
        double peso = input.nextDouble();
        System.out.print("Ingrese Estatura (m): ");
        double estatura = input.nextDouble();
        System.out.print("Tiene seguro(true)(false): ");
        boolean seguro = input.nextBoolean();

        //calculo de indice de masa corporal
        double imc = peso / (Math.pow(peso, 2));

        System.out.println("Los datos del paciente:");
        System.out.printf("%s%s%n%s%s%n%s%d%n%s%s%n%s%.2f%n%s%.2f%n%s%s%n",
                "Nombre: ", nombre,
                "Id: ", id,
                "Edad: ", edad,
                "Sexo: ", sexo,
                "Peso(kg): ", peso,
                "Estatura (m): ", estatura,
                "Seguro: ", seguro);
        System.out.printf("%s%.2f%n", "Indice de masa Corporal: ", imc);
        if (imc < 18.5) {
            System.out.println("Peso corporal: Bajo");
        } else if (imc > 18.5 && imc <= 24.99) {
            System.out.println("Peso corporal: Normal");
        } else {
            System.out.println("Peso Corporal: Alto");
        }

        if (edad < 12) {
            System.out.printf("%s%s%n%s%s%n",
                    "Paciente: ", nombre,
                    "Medico: ", "Pediatra");
        } else if (edad >= 12 && edad <= 60) {
            System.out.printf("%s%s%n%s%s%n",
                    "Paciente: ", nombre,
                    "Medico: ", "Medicina General");
        } else {
            System.out.printf("%s%s%n%s%s%n",
                    "Paciente: ", nombre,
                    "Medico: ", "Geriatra");
        }
    }//end method main
}//end class ConsultaMedica

package TrabajoAutonomo1;

import java.util.Scanner;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class PumGame {

    public static void main(String[] args) {
        System.out.println("CARLOS MERCHAN");

        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el numero: ");
        int numero = input.nextInt();
        for (int i = 1; i <= numero; i++) {
            if ((i % 7 == 0) || (i % 10 == 7)) {
                System.out.printf("%s ", "pum");
            } else {
                System.out.printf("%d ", i);
            }
        }//end for
        System.out.println("");
        input.close();
    }//end method main
}//end class PumGame

package TrabajoAutonomo1;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class BasicMath {

    public static void main(String[] args) {
        System.out.println("CARLOS MERCHAN");

        //Arithmetic using integers
        System.out.println("Integer Arithmetic");
        int a = 1 + 1;
        int b = a * 3;
        int c = b / 4;//entero dividido por otro entero da otro entero sin importar que
        //el resultado de 1.5. si se modifica el tipo de dato por double igual almacena 1.0
        //ya que la division es entre enteros
        int d = c - a;
        int e = -d;

        System.out.println("a= " + a);
        System.out.println("b= " + b);
        System.out.println("c= " + c);
        System.out.println("d= " + d);
        System.out.println("e= " + e);

        //Arithmetic using doubles
        System.out.println("Floating Point Arithmetic");
        double da = 1 + 1;
        double db = da * 3;
        double dc = db / 4;
        double dd = dc - a;
        double de = -dd;

        System.out.println("da= " + da);
        System.out.println("db= " + db);
        System.out.println("dc= " + dc);
        System.out.println("dd= " + dd);
        System.out.println("de= " + de);
    }//end method main
}//end class BasicMath

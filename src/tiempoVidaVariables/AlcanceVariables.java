package tiempoVidaVariables;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author aacarrer
 */
public class AlcanceVariables {
    // Demonstrate block scope.
    public static void main(String args[]) {
        int x=10; 
        
        if(x == 10) { // inicio de un nuevo scope
            int y = 20;
            // x and y son conocidas aqui.
            System.out.println("x and y: " + x + " " + y);
            x = y * 2;
        }
        
        //y fue declarado dentro del bloque if, por lo tanto solo es conocido dentro de ese bloque
        //System.out.println(y);
        
        //x fue inicializado dentro del bloque principal del metodo
        //por lo qu ees conocido dentro de todo el metodo
        System.out.println("x is " + x);
    }
}


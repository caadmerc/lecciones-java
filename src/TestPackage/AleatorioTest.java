/*
Clase que muestra como crear numeros Aleatorios

 */
package TestPackage;

import java.util.Random; //Para el metodo 1
import java.util.concurrent.ThreadLocalRandom;//Para el metodo 2
import java.security.SecureRandom;//Para el metodo 4

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class AleatorioTest {

    public static void main(String[] args) {
        /*
        Aleatorio entre [5,10]
        Metodo 1
        Random rd = new Random();
        int x=rd.nextInt(Max-Min+1)+Min;
         */

        Random rd = new Random();
        for (int i = 0; i < 10; i++) {
            System.out.println("Random1: " + (rd.nextInt(10 - 5 + 1) + 5));
        }

        //salto de linea
        System.out.println();

        /*
        Aleatorio entre [0,5]
        Metodo 2
        int randomNum=ThreadLocalRandom.current().nextInt(Min, Max+1);
        Se agrega 1 al Max para hacerlo inclusivo
         */
        for (int i = 0; i < 10; i++) {
            System.out.println("Random2: " + (ThreadLocalRandom.current().nextInt(0, 6)));
        }

        //salto de linea
        System.out.println();

        /*
        Aleatorio entre [1,10]
        Metodo 3
        int randomNum=(int)(Math.random()*(Max-Min+1))+Min;
         */
        for (int i = 0; i < 10; i++) {
            System.out.println("Random3: " + (int) ((Math.random() * (10 - 1 + 1)) + 1));
        }

        //salto de linea
        System.out.println();

        /*
        Aleatorio entre [1,5]
        Metodo 4
        SecureRandom rd=new SecureRandom();
        int randomNum=1+rd.nextInt(10); 
         */
        SecureRandom randomNumber = new SecureRandom();
        for (int i = 0; i < 10; i++) {
            System.out.println("Random4: " + (1 + randomNumber.nextInt(5)));
        }

        //metodo nextInt(int Maximo) retorna un entero desde 0 hasta el numero Maximo. [0,Maximo)
        //forma general: int randon=min+rd.nextInt(Maximo);
    }//end method main
}//end class AleatorioTest

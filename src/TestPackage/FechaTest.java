/*

 */
package TestPackage;


import java.time.LocalDate;//para prueba: 1,2,3,4
import java.time.ZoneId;//para prueba2
import java.time.format.DateTimeFormatter;//para prueba4
import java.time.DayOfWeek;//para mostrar el dia de la semana 
import java.time.Month;//para mostrar el mes
import java.time.DateTimeException;//para el error en la prueba4

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class FechaTest {

    public static void main(String[] args) {

        //prueba1
        //Obtiene la fecha actual en formato: yyyy-MM-dd
        LocalDate date1 = LocalDate.now();
        System.out.println("date = " + date1);

        //prueba2
        //Obtiene la fecha dada un timeZone en formato: yyyy-MM-dd
        //encuentra zonas en: https://docs.oracle.com/javase/8/docs/api/java/time/ZoneId.html
        LocalDate date2 = LocalDate.now(ZoneId.of("Australia/Sydney"));
        System.out.println("date2 = " + date2);

        //prueba3
        //convertir una fecha dada en String a LocalDate
        //solo es util si la fecha se encuentra en este formato, caso contrario usar metodo 4
        String fecha = "2018-04-13";
        LocalDate date3 = LocalDate.parse(fecha);
        System.out.println("date3 = " + date3);

        //prueba4
        //convertir una fecha dada en String a LocalDate 
        //en el DateTimeFormatter se especifica el formato de la fecha a convertir
        //LocalDate.parse convierte a LocalDate la fecha con el formato ingresado
        String fecha4 = "28/10/2018";
        try {
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate date4 = LocalDate.parse(fecha4, formato);
            System.out.println("date4 = " + date4);

            // imprime la fecha ingresada con su formato original
//        System.out.println("date4 = " + formato.format(date4)); 
            //compara dos fechas dadas 
            //retorna >0 si la fecha que llama al metodo es mayor que la del argumento
            //retorna <0 si la fecha que llama al metodo es menor que la del argumento
            //retorna =0 si la fecha que llama al metodo coincide con la del argumento
            System.out.println("Comparacion: " + date4.compareTo(LocalDate.now()));

            //uso del metodo isAfter
            System.out.println("Uso del isAfter " + (date4.isAfter(LocalDate.now())));

            //Prueba de Metodos getter
            int dayOfMonth = date4.getDayOfMonth();
            System.out.println("dayOfMonth = " + dayOfMonth);

            DayOfWeek dayOfWeek = date4.getDayOfWeek();
            System.out.println("dayOfWeek = " + dayOfWeek);

            int monthValue = date4.getMonthValue();
            System.out.println("monthValue = " + monthValue);

            Month month = date4.getMonth();
            System.out.println("month = " + month);

            int year = date4.getYear();
            System.out.println("year = " + year);
        } catch (DateTimeException e) {
            System.out.println(" " + e);
        }
    }//end method main
}//end class FechaTest

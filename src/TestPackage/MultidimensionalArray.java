package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class MultidimensionalArray {

    public static void main(String args[]) {
        int twoD[][] = new int[4][5];
        int k = 0;

        for (int i = 0; i < twoD.length; i++) {
            for (int j = 0; j < twoD[i].length; j++) {
                System.out.printf("%d\t", twoD[i][j] = k);
                k++;
            }
            System.out.println();
        }
    }//end method main
}//end class MultidimensionalArray

/*
Programa que ordena un arreglo de String de

 */
package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class OrdenarArrayString {

    public static void main(String[] args) {
        String[] array = {"Ecuador", "Zuiza", "Brazil", "Dinamarca", "Peru", "Argentina"};

        //muestra el arreglo de String en desorden
        System.out.println("Arreglo en desorden:");

        for (String x : array) {
            System.out.print("|" + x);
        }

        //Ordena de Menor a Mayor
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if ((array[i].compareToIgnoreCase(array[j])) > 0) {
                    String temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        //salto de linea
        System.out.println();

        //muestra el arreglo de String en orden
        System.out.println("Arreglo en orden:");

        for (String x : array) {
            System.out.print("|" + x);
        }

        //salto de linea
        System.out.println();

    }//end method main
}//end class OrdenarArrayString

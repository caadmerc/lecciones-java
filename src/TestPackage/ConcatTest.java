package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class ConcatTest {
    public static void main(String[] args) {
        String str="Carlos Adrian";
        String str2=" Merchan Anchundia";
        String strConcat=str.concat(str2);
        System.out.println("strConcat = " + strConcat);
    }
}

/*

 */
package TestPackage;

import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class CalendarTest {

    public static void main(String[] args) {
        //muestra fecha actual con formato metodo 1
        Date date1 = new Date();
        DateFormat dateFormat = new SimpleDateFormat("EEEE dd/MM/yy");
        String formattedDate = dateFormat.format(date1);
        System.out.println(formattedDate);

        /**
         * Muestra la hora de Ecuador
         */
        String a = dateFormat.getTimeZone().getDisplayName();
        System.out.println(a);

//        Pedir fecha por teclado
        Calendar calendar = new GregorianCalendar(2018, 9, 19);
//        int day = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.setTime(date1); // ingresa la fecha actual del sistema a calendar
        //ver que numero de dia es
        
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.SUNDAY:
                System.out.println("Sunday");
                break;
            case Calendar.MONDAY:
                System.out.println("Monday");
                break;
            case Calendar.TUESDAY:
                System.out.println("Tuesday");
                break;
            case Calendar.WEDNESDAY:
                System.out.println("Wednesday");
                break;
            case 5:
                System.out.println("Thursday");
                break;
            case Calendar.FRIDAY:
                System.out.println("Friday");
                break;
        }

        //muestra fecha actual metodo 2
//        Date date = Calendar.getInstance().getTime();
//        System.out.println(date);
        //muestra fecha actual metodo 3
//        Date date2 = new Date(); // date object
//        System.out.println(date2);
    }
}// end class TablaPosiciones

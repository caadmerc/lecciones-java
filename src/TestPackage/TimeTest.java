/*

 */
package TestPackage;

import java.time.format.DateTimeFormatter;
import java.time.LocalTime;
import java.time.DateTimeException;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class TimeTest {

    public static void main(String[] args) {

        //Metodo1: forma directa, ya que el formato esta de acuerdo al api de java
        try {
            String time2 = "23:50";
            LocalTime time = LocalTime.parse(time2);
            System.out.println("time2 = " + time);

            System.out.println("Hour: " + time.getHour());
            System.out.println("Minute: " + time.getMinute());
        } catch (DateTimeException e) {
            System.err.println(" " + e);
        }

        //Metodo2: aplicando formato
        try {
            String time = "22:00";
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("HH:mm");
            LocalTime time1 = LocalTime.parse(time, formato);
            System.out.println("hora = " + time1);

            //metodos para obtener los valores de la hora
            int hour = time1.getHour();
            System.out.println("hour = " + hour);

            int minutes = time1.getMinute();
            System.out.println("minutes = " + minutes);

        } catch (DateTimeException e) {
            System.err.println(" " + e);
        }

    }//end method main
}//end class TimeTest

package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class ContainsTest {
    public static void main(String[] args) {
        String str="Carlos Merchan Anchundia";
        System.out.println("boolean: "+str.contains("Carlos"));
        System.out.println("boolean: "+str.contains("ANCHUNDIA"));
        System.out.println("boolean: "+str.contains("os"));
        System.out.println("boolean: "+str.contains("M"));
        System.out.println("boolean: "+str.contains("e"));
        System.out.println("boolean: "+str.contains("R"));
        
        
    }//end method main
}//end class ContainsTest
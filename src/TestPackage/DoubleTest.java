package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class DoubleTest {

    public static void main(String[] args) {
        //representando un double con punto y coma
        double d = 123456.55;
        System.out.printf("double: %,.2f ", d);
    }
}

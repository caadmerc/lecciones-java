// Programa que prueba el uso de la sentencia de seleccion multiple: switch
// en el case 3 no se escribe el break, por lo tanto la secuencia sigue hasta encontrar
// en la siguiente condicion(entra al case 4) el break correspondiente.
package TestPackage;

import java.util.Scanner;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class TestSwitch {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese un entero: ");
        int entero = input.nextInt();

        switch (entero) {
            case 1:
                System.out.println("Ingreso1");
                break;
            case 2:
                System.out.println("Ingreso2");
                break;
            case 3:
                System.out.println("Ingreso3");
            case 4:
                System.out.println("Ingreso4");
                break;
            case 5:
                System.out.println("Ingreso5");
                break;
            default:
                System.out.println("Ningun valor valido");
        }//end switch
    }//end method main
}//end class TestSwitch

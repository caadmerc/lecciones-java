//Este metodo SOLO toma en cuenta que el string este separado por un solo tipo de 
//delimitador para poder separarlo. Caso contrario no lo separa.
package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class SplitTest {

    public static void main(String[] args) {
        String str = "esto:es:una:prueba:de:string";
        System.out.println("Uso del metodo split de la clase String:");
        System.out.println("str = " + str);

        String[] strSplit = str.split(":");
        for (String s : strSplit) {
            System.out.println(s);
        }
        System.out.printf("%s%d%n",
                "Tamaño del array generado por el metodo split: ",
                strSplit.length);

    }//end method main
}//end class SplitTest

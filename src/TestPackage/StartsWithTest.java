package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class StartsWithTest {
    public static void main(String[] args) {
        String str="Carlos Merchan Anchundia";
        System.out.println("boolean: "+str.startsWith("Merchan"));
        System.out.println("boolean: "+str.startsWith("Carlos"));
        System.out.println("boolean: "+str.startsWith("carlos"));
        System.out.println("boolean: "+str.startsWith("ca"));
        System.out.println("boolean: "+str.startsWith("os",4));
        System.out.println("boolean: "+str.startsWith("Ca"));
        System.out.println("boolean: "+str.startsWith("C"));
    }
}

/*
Programa que ordena un arreglo de enteros

 */
package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class OrdenarArrayEnteros {
    
    public static void main(String[] args) {
        int[] array = {9, 2, 7, 1, 3, 8};

        //Muestra el arreglo en desorden
        System.out.println("Arrreglo en desorden:");

        for (int x : array) {
            System.out.print(" " + x);
        }

        // Ordena de Menor a Mayor
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                   int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        //salto de linea
        System.out.println();

        //Muestra el arreglo en orden
        System.out.println("Arrreglo en orden:");
        for (int x : array) {
            System.out.print(" " + x);
        }

        //salto de linea
        System.out.println();

    }//end method main
}//end class OrdenarArrayEnteros

package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class SubstringTest {

    public static void main(String[] args) {
        String str = "Carlos Merchan Anchundia";
        String subStr = str.substring(6);
        System.out.println("subStr: " + subStr);
        System.out.println("substring: " + (str.substring(7)).toUpperCase());
        System.out.println("substring: " + (str.substring(0)).toLowerCase());
    }
}

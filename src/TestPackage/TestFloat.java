// Programa que muestra 2 numeros de tipo float con la caracteristica de que al final de 
// la inicializacion se agrega la letra f que indica que solo debe guardar hasta los
//valores ingresados antes de esta letra
package TestPackage;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class TestFloat {

    public static void main(String[] args) {
        float f1 = 0.1f;
        float f2 = 1.703f;
        System.out.println("Muestra 2 numeros de tipo float ingresados por teclado:");

        System.out.println("Float 1: " + f1);
        System.out.println("Float 2: " + f2);
    }//End method main
}//end class TestFloat

package TestPackage;

/**
 * @author Carlos Merchan Anchundia
 */
public class IndexOfTest {
    public static void main(String[] args) {
        String str="Carlos Merchan Anchundia";
        
        System.out.println("Encontrado: "+str.indexOf("Carlos"));
        System.out.println("Encontrado2: "+str.indexOf("os"));
        System.out.println("Encontrado3: "+str.indexOf('h'));
        System.out.println("Encontrado4: "+str.indexOf('a',3));
        System.out.println("Encontrado5: "+str.indexOf("CARLOS"));
        System.out.println("Encontrado6: "+str.indexOf('N'));
        
    }//end method main    
}//end class IndexOfTest
//Clase que muestra el uso de la clase del StringTokenizer 
package TestPackage;

import java.util.*;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class StringTokenizerTest {

    public static void main(String[] args) {
        String str = "esto;es:una,prueba/de:tokens";
        StringTokenizer token = new StringTokenizer(str, ":;,/");

        System.out.println("token = " + token.countTokens());
        while (token.hasMoreTokens()) {
            System.out.println("nextToken: " + token.nextToken());
        }

    }//end method main
}//end class StringTokenizer

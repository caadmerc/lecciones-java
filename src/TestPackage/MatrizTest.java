/*
Imprime una matriz ya definida usando el for anidado y el for each

 */
package POO;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class MatrizTest {

    public static void main(String[] args) {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(" " + matriz[i][j]);
            }
            System.out.println();
        }//end for

        System.out.println();

        for (int x[] : matriz) {
            for (int y : x) {
                System.out.print(" " + y);
            }
            System.out.println();
        }//end for

    }//end method main
}//end class MatrizTest

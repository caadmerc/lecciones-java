/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;
import java.util.Scanner;

/**
 *
 * @author rociomera
 */
public class Nomina {
    public static void main(String[] args){
        Scanner ingreso = new Scanner(System.in);
        
        System.out.print("Ingrese numero de empleados: ");
        int nEmpleados = ingreso.nextInt();
        
        double salarios[] = new double[nEmpleados];
        
        for(int i =0; i < nEmpleados ; i++){
            //pedir el ingreso del salario y almacenarlo 
            //en el arreglo salarios
            System.out.print("Ingrese salario empleado "+(i+1)+" :");
            salarios[i] = ingreso.nextDouble();
        }
        ingreso.close();
        
        final double SUELDO_BASICO = 375.00;
        for(int i =0; i < nEmpleados ; i++){
            
            double bono = 100;
            if(salarios[i]>SUELDO_BASICO){
                bono = salarios[i]*0.15;
            }
            
            System.out.printf("empleado %d sueldo: %,.3f bono: %,.3f %n",i+1,salarios[i],bono);
        }
    }//end method main
}//end class Nomina

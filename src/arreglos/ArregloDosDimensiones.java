/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author aacarrer
 */
public class ArregloDosDimensiones {
    public static void main(String args[]) {
        int twoD[][]= new int[4][5];
        int i, j, k = 0;
        for(i=0; i<twoD.length; i++){
            for(j=0; j<twoD[i].length; j++) {
                twoD[i][j] = k;
                k++;
            }
        }
        
        for(i=0; i<twoD.length; i++) {
            for(j=0; j<twoD[i].length; j++){
                System.out.print(twoD[i][j] + " ");
            }
            System.out.println();
        }
    }
}

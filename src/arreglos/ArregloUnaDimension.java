/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author aacarrer
 */
public class ArregloUnaDimension {

    public static void main(String args[]) {
        
        //metodo1 
        int month_days[] = new int[12]; //declaracion e instanciacion
        month_days[0] = 31; //modificar valores almacenados
        month_days[1] = 28;
        month_days[2] = 31;
        month_days[3] = 30;
        month_days[4] = 31;
        month_days[5] = 30;
        month_days[6] = 31;
        month_days[7] = 31;
        month_days[8] = 30;
        month_days[9] = 31;
        month_days[10] = 30;
        month_days[11] = 31;
        System.out.println("April has " + month_days[3] + " days.");
        
        //metodo 2
        int month_days2[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; //declaracion en inicilazacion
        System.out.println("April has " + month_days2[3] + " days.");
    }
}

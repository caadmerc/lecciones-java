/*
Ejemplo de la Unidad 2-1
crear un arreglo y lo llene con los cuadrados de cada indice

 */
package POO;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class Ejemplo1 {

    public static void main(String[] args) {
        final int DIM = 10;
        int[] arreglo = new int[DIM];
        for (int i = 0; i < arreglo.length; i++) {
            System.out.printf("Arreglo[%d]:%d\n",
                    i,
                    (arreglo[i] = i * i));
        }//end for
    }//end method main
}//end class Ejemplo1

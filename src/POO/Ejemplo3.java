/*
Ejemplo de la Unidad 2-1
Imprime los elementos del arreglo separandolos con el simbolo |

 */
package POO;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class Ejemplo3 {

    public static void main(String[] args) {
        final int DIM = 10;
        int[] arreglo = new int[DIM];
        for (int i = 0; i < arreglo.length; i++) {
            System.out.printf("%d | ",
                    (arreglo[i] = i * i));
        }//end for
        System.out.println();

    }//end method main
}//end class Ejemplo3

/*
*Tarea#1
*Programacion Orientada a Objetos
*Paralelo 6
 */
package POO;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class Tarea1Merchan {

    public static void main(String[] args) {
        final int DIM = 10;
        int suma = 0;
        double promedio;
        int[] arreglo = new int[DIM];

        //Parte 1
        //Arreglo de 10 elementos
        System.out.println("Parte 1");
        for (int i = 0; i < arreglo.length; i++) {
            System.out.printf("Arreglo[%d]:%d\n",
                    i,
                    (arreglo[i] = i * i));
            suma += arreglo[i];
        }//end for

        //Parte 2
        System.out.println("Parte 2");
        promedio = (double) suma / DIM; //cast & promotion
        System.out.printf("%s%d\n%s%.2f\n",
                "suma:", suma,
                "promedio:", promedio);

        //Parte 3 
        System.out.println("Parte 3");
        for (int x : arreglo) {
            System.out.print("|" + x);
        }

        //salto de linea
        System.out.println();
    }//end method main
}//Tarea1Merchan

/*
Ejemplo de la Unidad 2-1
Programa que calcula la suma y el promedio de los elementos del arreglo

 */
package POO;

/**
 *
 * @author Carlos Merchan Anchundia
 */
public class Ejemplo2 {

    public static void main(String[] args) {
        final int DIM = 10;
        int suma = 0;
        double promedio;
        int[] arreglo = new int[DIM];
        for (int i = 0; i < 10; i++) {
            arreglo[i] = i * i;
            suma += arreglo[i];
        }//end for

        promedio = (double) suma / DIM;
        System.out.printf("%s%d\n%s%.2f\n",
                "suma:", suma,
                "promedio:", promedio);

    }//end method main
}// end class Ejemplo2

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasdecontrol;

/**
 *
 * @author aacarrer
 */
public class EjemploDoWhile {
    public static void main(String args[]) {
        int n = 10;
        
        do{
            System.out.println("n: "+n);
            n--;
        }while(n>0);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasdecontrol;
import java.util.Scanner;

/**
 * Programa que muestra el valor a pagar por concepto de peaje dependiendo del
 * tipo de vehiclo
 * @author Rocio Mera
 */
public class EjemploCombinado {
    public static void main(String args[]) {
        int claseDeVehiculo;
        double valorPeaje = 0;
        System.out.println("Ingrese el tipo de vehiculo");
        
        String[] tiposVehiculos={"Carro de Pasajeros","Bus","Camion","Vehiculo desconocido"};
        for(int i=0;i<tiposVehiculos.length;i++){
            System.out.printf("%d %s %n",i+1,tiposVehiculos[i]);
        }
        
        Scanner entrada = new Scanner(System.in);
        
        claseDeVehiculo = entrada.nextInt();
        
        switch (claseDeVehiculo)
        {
            case 1:
                System.out.println("Carro de Pasajeros.");
                valorPeaje = 0.50;
                break ;
            case 2:
                System.out.println("Bus.");
                valorPeaje = 1.50;
                break ;
            case 3:
                System.out.println("Camión.");
                valorPeaje = 2.00;
                break ;
            default :
                System.out.println("Vehiculo desconocido!");
                break ;
        }
        
        if (valorPeaje==0.0) {
            System.out.println("Vehiculo inválido");
        } else {
            System.out.printf("El valor del peaje a pagar es %.2f%n",(valorPeaje));
        }
    }//end method main
}//end class EjemploCombinado

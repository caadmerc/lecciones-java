/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasdecontrol;

import java.util.Scanner;
public class EjemploSwitch {
    public static void main(String args[]) {
        
        System.out.println("Ingrese tipo de fruta");
        Scanner entrada = new Scanner(System.in);
        int key = entrada.nextInt();
        
        switch (key){
            case 1:
                System.out.println("Apples");
                System.out.println("Apples");
                break ;
            case 2:
                System.out.println("Oranges");
                break ;
            case 3:
                System.out.println("Peaches");
                //break ; //que pasa si quito la sentencia break
            case 4:
                System.out.println("Plums");
                break; 
            default :
                System.out.println("Fruitless");
                break;
        }
        
    }//end method main
}//end class EjemploSwitch

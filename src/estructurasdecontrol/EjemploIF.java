/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasdecontrol;

/**
 *
 * @author aacarrer
 */
public class EjemploIF {

    public static void main(String args[]) {
        int x = 10, y = 20;
        if (x < y) {
            System.out.println("x es menor que y");
        }

        x = x * 2;
        if (x == y) {
            System.out.println("x es igual a  y");
        }

        x = x * 2;
        if (x > y) {
            System.out.println("x es mayor que y");
        }

        // this won't display anything
        if (x == y) {
            System.out.println("no se cumple la condición no se ejecuta");
        }
    }//end method main
}//end class EjemploIF

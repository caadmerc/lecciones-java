/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasdecontrol;

/**
 *
 * @author aacarrer
 */
public class EjemploFor {

    public static void main(String args[]) {
        int x;
        for (x = 0; x < 10; x++) {
            System.out.println("This is x: " + x);
            x++;
        }

        System.out.println("This is x: " + x);
    }//end method main
}//end class EjemploFor
